package org.kohsuke.github;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.Date;


public class gitus {

	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		GitHub github = GitHub.connectUsingPassword("hughwarren","a0123456");
	    GHMyself me = github.getMyself();
	    PagedIterable<GHRepository> lstRepos = me.listRepositories();
	    int repoCounter = 0;
	    PrintWriter outRepoList = new PrintWriter("repo_list.txt");
	    for (GHRepository repo : lstRepos) {
	        String name = repo.getName();
	        Date pushedAt = repo.getPushedAt();
	        String descrip = repo.getDescription();
	        int issues = repo.getOpenIssueCount();
	        outRepoList.format("%s \t %s \t %s \t %d \n", name, descrip, pushedAt, issues);
	        repoCounter++;
	    }

	    outRepoList.close();
	    
		System.out.println("Name is: " + me.name);
		System.out.println("Company is: " + me.getCompany());
		System.out.println("Email is: " + me.getEmail());
		System.out.println(repoCounter + " repositories have been saved to repo_list.txt\n");
	}

}
