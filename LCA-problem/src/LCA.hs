module LCA where

import Text.Printf

data Tree a = EmptyNode | Node a (Tree a) (Tree a) deriving Show

myTree :: Tree Int
myTree = Node 1 
    (Node 2 
        (Node 4 EmptyNode EmptyNode)
        (Node 5 EmptyNode EmptyNode))
    (Node 3 
        (Node 6 EmptyNode EmptyNode)
        (Node 7 EmptyNode EmptyNode))


lcat :: Eq a => Tree a -> a -> a -> Either Bool a
lcat EmptyNode _ _ = Left False
lcat (Node v tl tr) n1 n2 = 
    let l = lcat tl n1 n2
        r = lcat tr n1 n2
        onroot = v == n1 || v == n2
    in case (l, r, onroot) of
        (Right a  , _         , _    ) -> Right a
        (_        , Right a   , _    ) -> Right a
        (Left True, Left True , _    ) -> Right v
        (Left True, _         , True ) -> Right v
        (_        , Left True , True ) -> Right v
        (Left True, _         , False) -> Left True
        (_         , Left True, False) -> Left True
        (_         , _        , True ) -> Left True
        _ -> Left False

