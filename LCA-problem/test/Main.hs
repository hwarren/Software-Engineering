{-# LANGUAGE StandaloneDeriving #-}

module Main where

import Test.HUnit
import Test.Framework.Providers.HUnit (testCase)
import LCA

{- HUnit Tests -}


test1 =  TestCase (assertEqual "for (lcat 4,5)," (2) (lcat 4,5)) 
test2 =  TestCase (assertEqual "for (lcat 0,0)," (-1) (lcat 0,0)) 
test3 =  TestCase (assertEqual "for (lcat 100,100)," (-1) (lcat 100,100)) 
test4 =  TestCase (assertEqual "for (lcat 3,4)," (1) (lcat 3,4)) 


tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2,TestLabel "test3" test3,TestLabel "test4" test4]